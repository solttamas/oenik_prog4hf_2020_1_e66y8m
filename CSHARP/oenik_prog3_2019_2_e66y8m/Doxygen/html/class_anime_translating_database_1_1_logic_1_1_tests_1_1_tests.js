var class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests =
[
    [ "TestingTheAddingOfACompany", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a7f3b44797755be7fa88621f746d3b63b", null ],
    [ "TestingTheAddingOfATranslator", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a6e677d8089ee9c19225d6dbeb03191f6", null ],
    [ "TestingTheAnimesAboveEightScoreMethod", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a5a41610ce57eed561ffd0d922be76fae", null ],
    [ "TestingTheCompanyTranslatorCountMethod", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a14df08436b00968a3e4810eb6edda59d", null ],
    [ "TestingTheDeletingOfACompany", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a76b09758f60e5fa6e72ee5ab4073a8ce", null ],
    [ "TestingTheDeletingOfAnAnime", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a052e539e7cf47b5d6549454032b89ffd", null ],
    [ "TestingTheDeletingOfATranslator", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a19c9f9e6894107e573563e9a15bfd88b", null ],
    [ "TestingTheEditingOfACompany", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a42d72c401ae1e1472384c364fc53dd4b", null ],
    [ "TestingTheEditingOfATranslator", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#afc5a6f2213ee56348b942420cb72b75f", null ],
    [ "TestingTheJapaneseTranslatorsFromDavidProductionZrtMethod", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a524e31066805d7edf8d840b3d2b82054", null ],
    [ "TestingTheListingOfCompanies", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#ab8c869c3012e12e1d2360a6a5706fe12", null ],
    [ "TestingTheListingOfTranslators", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a83085885c267ae9d304f5939a7102c8d", null ],
    [ "TestingTheTranslatorsAndAnimeMethod", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a270dc0812552aca05b6664e3c05fd061", null ],
    [ "TestingTheYoungTranslatorsMethod", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html#a9f6f9df7ee6958a66e575bc69211a486", null ]
];