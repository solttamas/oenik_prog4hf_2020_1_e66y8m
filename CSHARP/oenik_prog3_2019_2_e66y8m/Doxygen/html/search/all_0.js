var searchData=
[
  ['anime_0',['Anime',['../class_anime_translating_database_1_1_data_1_1_anime.html',1,'AnimeTranslatingDatabase::Data']]],
  ['animedatabasemodel_1',['AnimeDatabaseModel',['../class_anime_translating_database_1_1_data_1_1_anime_database_model.html',1,'AnimeTranslatingDatabase::Data']]],
  ['animesaboveeightscore_2',['AnimesAboveEightScore',['../interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a7347d7b3c3ccb5129aa9c7213b35e355',1,'AnimeTranslatingDatabase.Logic.ILogic.AnimesAboveEightScore()'],['../class_anime_translating_database_1_1_logic_1_1_logic.html#ad5664c8a09f655bd86cfd4e6bee3814a',1,'AnimeTranslatingDatabase.Logic.Logic.AnimesAboveEightScore()']]],
  ['animetranslatingdatabase_3',['AnimeTranslatingDatabase',['../namespace_anime_translating_database.html',1,'']]],
  ['data_4',['Data',['../namespace_anime_translating_database_1_1_data.html',1,'AnimeTranslatingDatabase']]],
  ['logic_5',['Logic',['../namespace_anime_translating_database_1_1_logic.html',1,'AnimeTranslatingDatabase']]],
  ['program_6',['Program',['../namespace_anime_translating_database_1_1_program.html',1,'AnimeTranslatingDatabase']]],
  ['repository_7',['Repository',['../namespace_anime_translating_database_1_1_repository.html',1,'AnimeTranslatingDatabase']]],
  ['tests_8',['Tests',['../namespace_anime_translating_database_1_1_logic_1_1_tests.html',1,'AnimeTranslatingDatabase::Logic']]]
];
