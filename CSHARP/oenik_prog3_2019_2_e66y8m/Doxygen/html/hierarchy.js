var hierarchy =
[
    [ "AnimeTranslatingDatabase.Data.Anime", "class_anime_translating_database_1_1_data_1_1_anime.html", null ],
    [ "AnimeTranslatingDatabase.Data.Company", "class_anime_translating_database_1_1_data_1_1_company.html", null ],
    [ "DbContext", null, [
      [ "AnimeTranslatingDatabase.Data.AnimeDatabaseModel", "class_anime_translating_database_1_1_data_1_1_anime_database_model.html", null ]
    ] ],
    [ "AnimeTranslatingDatabase.Program.DisplayConsole", "class_anime_translating_database_1_1_program_1_1_display_console.html", null ],
    [ "AnimeTranslatingDatabase.Logic.ILogic", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html", [
      [ "AnimeTranslatingDatabase.Logic.Logic", "class_anime_translating_database_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "AnimeTranslatingDatabase.Repository.IRepository", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html", [
      [ "AnimeTranslatingDatabase.Repository.Repository", "class_anime_translating_database_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "AnimeTranslatingDatabase.Program.Program", "class_anime_translating_database_1_1_program_1_1_program.html", null ],
    [ "AnimeTranslatingDatabase.Logic.Tests.Tests", "class_anime_translating_database_1_1_logic_1_1_tests_1_1_tests.html", null ],
    [ "AnimeTranslatingDatabase.Data.Translator", "class_anime_translating_database_1_1_data_1_1_translator.html", null ]
];