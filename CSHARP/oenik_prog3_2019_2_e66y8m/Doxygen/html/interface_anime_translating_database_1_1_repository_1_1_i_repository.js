var interface_anime_translating_database_1_1_repository_1_1_i_repository =
[
    [ "DeleteAnime", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a20c2af64d7843bbad3c39785f45639b4", null ],
    [ "DeleteCompany", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a8dd7b300ced23308f02233e8e9d351e3", null ],
    [ "DeleteTranslator", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#ae74878f1304117b038ead09ef9c7a117", null ],
    [ "EditAnime", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a48292f35891419ec9d81d3d794ba2167", null ],
    [ "EditCompany", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a613805808182e255bfac07233d9e3dde", null ],
    [ "EditTranslator", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#acace41f0abcd257f6ca243f33f2ec8df", null ],
    [ "GetAnimes", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a7ed4f4b3eb3e535b2843cd5af73c0323", null ],
    [ "GetCompanies", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a084a0fec2bf18c875145381441d3b181", null ],
    [ "GetTranslators", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#aae8946749d23ef5cf92f008a4a16c08c", null ],
    [ "InsertAnime", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a9d019ab78f69d9fbb6ba886007fe7747", null ],
    [ "InsertCompany", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#a8bb2b45d0cc24ca2c4163fedc0590904", null ],
    [ "InsertTranslator", "interface_anime_translating_database_1_1_repository_1_1_i_repository.html#ae309677dae1367ee03d88f7a4e1b1f6f", null ]
];