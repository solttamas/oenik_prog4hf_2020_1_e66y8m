var namespace_anime_translating_database_1_1_data =
[
    [ "Anime", "class_anime_translating_database_1_1_data_1_1_anime.html", "class_anime_translating_database_1_1_data_1_1_anime" ],
    [ "AnimeDatabaseModel", "class_anime_translating_database_1_1_data_1_1_anime_database_model.html", "class_anime_translating_database_1_1_data_1_1_anime_database_model" ],
    [ "Company", "class_anime_translating_database_1_1_data_1_1_company.html", "class_anime_translating_database_1_1_data_1_1_company" ],
    [ "Translator", "class_anime_translating_database_1_1_data_1_1_translator.html", "class_anime_translating_database_1_1_data_1_1_translator" ]
];