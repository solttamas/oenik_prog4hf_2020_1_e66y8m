var interface_anime_translating_database_1_1_logic_1_1_i_logic =
[
    [ "AnimesAboveEightScore", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a7347d7b3c3ccb5129aa9c7213b35e355", null ],
    [ "CompanyTranslatorCount", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a99160222e20f4e912966ffd67c8ad6c4", null ],
    [ "DeleteAnime", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#aa3487089996759f3fe338bc3914e260b", null ],
    [ "DeleteCompany", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#af5018ed5c507a5102cbe8d25d1b599ca", null ],
    [ "DeleteTranslator", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a606e2be2c26c1b651f6467483433f6fd", null ],
    [ "EditAnime", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a93b25c2d52e50d908e749c9cc9a20a55", null ],
    [ "EditCompany", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a7e8005f43cc910f022ef650182e3e7e4", null ],
    [ "EditTranslator", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a146e08883b508267e3e81b86048a600c", null ],
    [ "InsertAnime", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a3aa8ef7aa71279d63e92024f22b6bdaa", null ],
    [ "InsertCompany", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#af47adf5d94603382ae799107d9d57186", null ],
    [ "InsertTranslator", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a0077def4f7a215db686933584f6fa4af", null ],
    [ "JapaneseTranslatorsFromDavidProductionZrt", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a621afb743ffad8c6f5a181d0f184179a", null ],
    [ "Java", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a783c637774bdd974a11a9bb33f79a46d", null ],
    [ "StringAnimes", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a469fbb55fce0c7a26f0d15e9bc80a583", null ],
    [ "StringCompanies", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#ae6db0f8b8133d83a0f43c7590114841b", null ],
    [ "StringTranslators", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a5a455f9514623a265b698e902943caf0", null ],
    [ "TranslatorsAndAnimes", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a9bbfd02441687dfb2ee6185a812a8923", null ],
    [ "YoungTranslators", "interface_anime_translating_database_1_1_logic_1_1_i_logic.html#a9ceff3ca8d4139caa1dce90ef0f54efe", null ]
];