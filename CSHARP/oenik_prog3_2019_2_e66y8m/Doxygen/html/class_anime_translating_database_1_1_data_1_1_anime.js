var class_anime_translating_database_1_1_data_1_1_anime =
[
    [ "Anime_ID", "class_anime_translating_database_1_1_data_1_1_anime.html#a4d92b085cc8d16e321f4c13adf9af667", null ],
    [ "Episodes", "class_anime_translating_database_1_1_data_1_1_anime.html#ab0bc9f9b438832a47bd95ad5c2d916a4", null ],
    [ "Name", "class_anime_translating_database_1_1_data_1_1_anime.html#a039b4163edcdfd75808534ec31986035", null ],
    [ "Release_Date", "class_anime_translating_database_1_1_data_1_1_anime.html#ad5bf7d2b8626d3bc9657ba4aa4e0087d", null ],
    [ "Score", "class_anime_translating_database_1_1_data_1_1_anime.html#ab6537ac5a612f6976cd925632dc8b8ad", null ],
    [ "Translator", "class_anime_translating_database_1_1_data_1_1_anime.html#aa022daf43fb18052f2448a9455688ba8", null ],
    [ "Translator_ID", "class_anime_translating_database_1_1_data_1_1_anime.html#a4e2b68901c6dd1912b546ab548d8e257", null ],
    [ "Type", "class_anime_translating_database_1_1_data_1_1_anime.html#af87d6b4ef2c7aee9e89d777386238ee6", null ]
];