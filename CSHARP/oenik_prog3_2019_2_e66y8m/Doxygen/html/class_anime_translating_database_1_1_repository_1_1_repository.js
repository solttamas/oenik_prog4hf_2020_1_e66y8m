var class_anime_translating_database_1_1_repository_1_1_repository =
[
    [ "DeleteAnime", "class_anime_translating_database_1_1_repository_1_1_repository.html#afb310c1b1eea2d5940ec1eeefb25e69e", null ],
    [ "DeleteCompany", "class_anime_translating_database_1_1_repository_1_1_repository.html#ab643ed73d25d0c129856266d814de8c0", null ],
    [ "DeleteTranslator", "class_anime_translating_database_1_1_repository_1_1_repository.html#a4b4fef0dce2690d5f562956fd029615a", null ],
    [ "EditAnime", "class_anime_translating_database_1_1_repository_1_1_repository.html#a3856ea5231aeceef0c7bd2e45be84f88", null ],
    [ "EditCompany", "class_anime_translating_database_1_1_repository_1_1_repository.html#a10258a26f3696d1443470361bf706117", null ],
    [ "EditTranslator", "class_anime_translating_database_1_1_repository_1_1_repository.html#a982e49b93d118701065a1706ca0e2031", null ],
    [ "GetAnimes", "class_anime_translating_database_1_1_repository_1_1_repository.html#a5e658a27462c7d5912a6409832186afa", null ],
    [ "GetCompanies", "class_anime_translating_database_1_1_repository_1_1_repository.html#a0a3a8faec8f42ab6840e2d8745acacd3", null ],
    [ "GetTranslators", "class_anime_translating_database_1_1_repository_1_1_repository.html#af8e4106f29c9a4597097c41608fac6cf", null ],
    [ "InsertAnime", "class_anime_translating_database_1_1_repository_1_1_repository.html#a3eeca593a91ce30422b4126ac2fbe35c", null ],
    [ "InsertCompany", "class_anime_translating_database_1_1_repository_1_1_repository.html#a904c5208959945f86b388f41b83fc977", null ],
    [ "InsertTranslator", "class_anime_translating_database_1_1_repository_1_1_repository.html#ac45080553faefca6177b1866c5ce84d2", null ]
];