var class_anime_translating_database_1_1_data_1_1_anime_database_model =
[
    [ "AnimeDatabaseModel", "class_anime_translating_database_1_1_data_1_1_anime_database_model.html#af0cf6eed3b0029e930cbb27e58f48907", null ],
    [ "OnModelCreating", "class_anime_translating_database_1_1_data_1_1_anime_database_model.html#a18f094b840cce918105b63f7c7d69973", null ],
    [ "Animes", "class_anime_translating_database_1_1_data_1_1_anime_database_model.html#a9264313f8043494f6a31e97c4c082521", null ],
    [ "Companies", "class_anime_translating_database_1_1_data_1_1_anime_database_model.html#a87c6fd18305843261364f8a2506a7c1b", null ],
    [ "Translators", "class_anime_translating_database_1_1_data_1_1_anime_database_model.html#ada1622fdb4e08e15b67963064b645394", null ]
];