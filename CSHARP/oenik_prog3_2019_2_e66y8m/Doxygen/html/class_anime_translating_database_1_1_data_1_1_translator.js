var class_anime_translating_database_1_1_data_1_1_translator =
[
    [ "Translator", "class_anime_translating_database_1_1_data_1_1_translator.html#ae84319fa5c143dc9fe0fe12382e96e26", null ],
    [ "Animes", "class_anime_translating_database_1_1_data_1_1_translator.html#afb1ad6221522a9e98e8e87eb2a79d4f0", null ],
    [ "Birth", "class_anime_translating_database_1_1_data_1_1_translator.html#a434ecd6e182b86e488dbbed048db8177", null ],
    [ "Company", "class_anime_translating_database_1_1_data_1_1_translator.html#ae660384f5f6b96b562a02aff28ac62e7", null ],
    [ "Company_ID", "class_anime_translating_database_1_1_data_1_1_translator.html#aa329f8f42fc3d236991868617854d56e", null ],
    [ "Name", "class_anime_translating_database_1_1_data_1_1_translator.html#a09c39eef9b53ee55eb21f39e5c8baae5", null ],
    [ "Source_Language", "class_anime_translating_database_1_1_data_1_1_translator.html#a5a4d1fb57b5b8b27b01e9c43adb7bc28", null ],
    [ "Target_Language", "class_anime_translating_database_1_1_data_1_1_translator.html#a39582897d14e340b742742f996570884", null ],
    [ "Translator_ID", "class_anime_translating_database_1_1_data_1_1_translator.html#acb51a0ce7833e6290cf90bfdce227293", null ]
];