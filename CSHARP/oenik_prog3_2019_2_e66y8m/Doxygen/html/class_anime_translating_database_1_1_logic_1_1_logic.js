var class_anime_translating_database_1_1_logic_1_1_logic =
[
    [ "Logic", "class_anime_translating_database_1_1_logic_1_1_logic.html#a6b7178d69d0c4533eb7aff7de5bc7d8c", null ],
    [ "Logic", "class_anime_translating_database_1_1_logic_1_1_logic.html#ad7964e3abaeae88f2f54433274df649c", null ],
    [ "AnimesAboveEightScore", "class_anime_translating_database_1_1_logic_1_1_logic.html#ad5664c8a09f655bd86cfd4e6bee3814a", null ],
    [ "CompanyTranslatorCount", "class_anime_translating_database_1_1_logic_1_1_logic.html#af07a1870af9b7cff5b6cf5e53ea87cf8", null ],
    [ "DeleteAnime", "class_anime_translating_database_1_1_logic_1_1_logic.html#aa33bf37ff3eafa80a6fa9b81618dab35", null ],
    [ "DeleteCompany", "class_anime_translating_database_1_1_logic_1_1_logic.html#a76cf4e8423dda150f7403fe724b64c22", null ],
    [ "DeleteTranslator", "class_anime_translating_database_1_1_logic_1_1_logic.html#a2a0f40f5384b24a1e8f56ceb3dfd6c95", null ],
    [ "EditAnime", "class_anime_translating_database_1_1_logic_1_1_logic.html#ac2f89742e43d00afe1a4817fc0b531b1", null ],
    [ "EditCompany", "class_anime_translating_database_1_1_logic_1_1_logic.html#a954e717675307984d807e55065330027", null ],
    [ "EditTranslator", "class_anime_translating_database_1_1_logic_1_1_logic.html#a182ef95739008a4251291b18b4c0e19e", null ],
    [ "InsertAnime", "class_anime_translating_database_1_1_logic_1_1_logic.html#adbe025611f80e42382e2f6b5f8aaf466", null ],
    [ "InsertCompany", "class_anime_translating_database_1_1_logic_1_1_logic.html#a7dd561b7d7008cbe46af13e8992fd3f5", null ],
    [ "InsertTranslator", "class_anime_translating_database_1_1_logic_1_1_logic.html#a4248990dac46e8902ea34b553b3b93d2", null ],
    [ "JapaneseTranslatorsFromDavidProductionZrt", "class_anime_translating_database_1_1_logic_1_1_logic.html#adf61b86c54d2c723c4a00db544904751", null ],
    [ "Java", "class_anime_translating_database_1_1_logic_1_1_logic.html#ad9bb22d9d8ee8447739e74cac43b1453", null ],
    [ "StringAnimes", "class_anime_translating_database_1_1_logic_1_1_logic.html#a62ce0aad2c665a3f4a82451866529e24", null ],
    [ "StringCompanies", "class_anime_translating_database_1_1_logic_1_1_logic.html#ae0fd6983a577ddd9c117b850f5b3f3e2", null ],
    [ "StringTranslators", "class_anime_translating_database_1_1_logic_1_1_logic.html#a8b33474ab667b2ebd65d418e8141d490", null ],
    [ "TranslatorsAndAnimes", "class_anime_translating_database_1_1_logic_1_1_logic.html#a80ddcdb448cb06dec4ef024d6505c6ca", null ],
    [ "YoungTranslators", "class_anime_translating_database_1_1_logic_1_1_logic.html#a33445220d17ad87377610874e2bb898d", null ]
];