﻿// <copyright file="DisplayConsole.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AnimeTranslatingDatabase.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using AnimeTranslatingDatabase.AnimeLogic;

    /// <summary>
    /// The class for display data and the menu.
    /// </summary>
    public class DisplayConsole
    {
        /// <summary>
        /// The logic class.
        /// </summary>
        private Logic logic = new Logic();

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayConsole"/> class.
        /// Displays the menu and datas.
        /// </summary>
        public DisplayConsole()
        {
            ConsoleKey pressedKey;
            bool menuActive = true;

            while (menuActive == true)
            {
                this.MainMenu();
                pressedKey = Console.ReadKey().Key;
                switch (pressedKey)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        // ListData.
                        Console.Clear();
                        this.ListData();
                        Thread.Sleep(500);
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        // AddEntity.
                        Console.Clear();
                        try
                        {
                            this.AddEntity();
                        }
                        catch (IndexOutOfRangeException)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the adding:");
                            Console.WriteLine("Probably the format of the data was wrong.");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (FormatException)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the adding:");
                            Console.WriteLine("Please put date for the date field, integer for the integer field, ect..");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (Exception)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the adding:");
                            Console.WriteLine("Unexpected error.");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        finally
                        {
                            Thread.Sleep(1000);
                        }

                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        // DeleteEntity.
                        Console.Clear();
                        try
                        {
                            this.DeleteEntity();
                        }
                        catch (InvalidOperationException)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the deleting:");
                            Console.WriteLine("Probably there is no matching data");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (Exception)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the deleting:");
                            Console.WriteLine("Unexpected error.");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        finally
                        {
                            Thread.Sleep(1000);
                        }

                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:
                        // UpdateEntity.
                        Console.Clear();
                        try
                        {
                            this.UpdateEntity();
                        }
                        catch (FormatException)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the updating:");
                            Console.WriteLine("Please put date for the date field, integer for the integer field, ect..");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (InvalidOperationException)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the updating:");
                            Console.WriteLine("Probably there is no matching data");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (Exception)
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with the updating:");
                            Console.WriteLine("Unexpected error.");
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                        finally
                        {
                            Thread.Sleep(1000);
                        }

                        break;
                    case ConsoleKey.D5:
                    case ConsoleKey.NumPad5:
                        // Queries.
                        Console.Clear();
                        this.Queries();
                        Thread.Sleep(500);
                        break;
                    case ConsoleKey.D6:
                    case ConsoleKey.NumPad6:
                        // Java.
                        Console.Clear();
                        try
                        {
                            Console.WriteLine(this.logic.Java());
                        }
                        catch (System.Net.WebException)
                        {
                            Console.WriteLine("Something went wrong with the server");
                        }
                        finally
                        {
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                            Thread.Sleep(1000);
                        }

                        break;
                    case ConsoleKey.Escape: Console.Clear(); menuActive = false; Console.WriteLine("Goodbye!"); Thread.Sleep(500); break;
                    default: Console.Clear(); Console.WriteLine("Wrong move"); Thread.Sleep(500); break;
                }
            }
        }

        /// <summary>
        /// Menu for the main page.
        /// </summary>
        private void MainMenu()
        {
            Console.Clear();
            string menu = "Anime translating database menu:\n" +
                            "1 - List all entities\n" +
                            "2 - Add entity\n" +
                            "3 - Delete entity\n" +
                            "4 - Update entity\n" +
                            "5 - Queries\n" +
                            "6 - Request for java information\n" +
                            "ESC - Exit\n" +
                            "Choose one: ";
            Console.Write(menu);
        }

        /// <summary>
        /// List the table's data.
        /// </summary>
        private void ListData()
        {
            ConsoleKey pressedKey;
            bool menuActive = true;

            while (menuActive == true)
            {
                this.ListMenu();
                pressedKey = Console.ReadKey().Key;
                switch (pressedKey)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        Console.Clear();
                        Console.WriteLine(this.logic.StringAnimes());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        Console.Clear();
                        Console.WriteLine(this.logic.StringCompanies());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        Console.Clear();
                        Console.WriteLine(this.logic.StringTranslators());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.Escape: menuActive = false; Console.WriteLine("\nBack to the future!"); break;
                    default: Console.Clear(); Console.WriteLine("Wrong move"); Thread.Sleep(1000); break;
                }
            }
        }

        /// <summary>
        /// Menu for the listing methods.
        /// </summary>
        private void ListMenu()
        {
            Console.Clear();
            string menu = "Choose one from below:\n" +
                            "1 - List all anime\n" +
                            "2 - List all company\n" +
                            "3 - List all translator\n" +
                            "ESC - Back\n" +
                            "Choose one: ";
            Console.Write(menu);
        }

        /// <summary>
        /// Add entity to a table.
        /// </summary>
        private void AddEntity()
        {
            ConsoleKey pressedKey;
            bool menuActive = true;

            while (menuActive == true)
            {
                this.AddMenu();
                pressedKey = Console.ReadKey().Key;
                switch (pressedKey)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        Console.Clear();
                        Console.WriteLine("Please type in the anime (with _ as separator) that you want to insert\n" +
                                          "(Name_ReleaseAsYYYY-MM-DD_NumberOfEpisodes_Type_ScoreAs*.**_TranslatorID)");
                        this.logic.InsertAnime(Console.ReadLine());
                        Console.WriteLine("Anime inserted succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        Console.Clear();
                        Console.WriteLine("Please type in the company (with _ as separator) that you want to insert\n" +
                                          "(Name_Location)");
                        this.logic.InsertCompany(Console.ReadLine());
                        Console.WriteLine("Company inserted succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        Console.Clear();
                        Console.WriteLine("Please type in the translator (with _ as separator) that you want to insert\n" +
                                          "(Name_BirthAsYYYY-MM-DD_CompanyID_SourceLanguage_TargetLanguage)");
                        this.logic.InsertTranslator(Console.ReadLine());
                        Console.WriteLine("Translator inserted succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.Escape: menuActive = false; Console.WriteLine("\nBack to the future!"); break;
                    default: Console.Clear(); Console.WriteLine("Wrong move"); Thread.Sleep(1000); break;
                }
            }
        }

        /// <summary>
        /// Menu for the adding methods.
        /// </summary>
        private void AddMenu()
        {
            Console.Clear();
            string menu = "Choose one from below:\n" +
                            "1 - Add new Anime in the database\n" +
                            "2 - Add new Company in the database\n" +
                            "3 - Add new Translator in the database\n" +
                            "ESC - Back\n" +
                            "Choose one: ";
            Console.Write(menu);
        }

        /// <summary>
        /// Delete entity from a table.
        /// </summary>
        private void DeleteEntity()
        {
            ConsoleKey pressedKey;
            bool menuActive = true;

            while (menuActive == true)
            {
                this.DeleteMenu();
                pressedKey = Console.ReadKey().Key;
                switch (pressedKey)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        Console.Clear();
                        Console.WriteLine("Please enter the anime's name you want to delete");
                        this.logic.DeleteAnime(Convert.ToInt32(Console.ReadLine()));
                        Console.WriteLine("Anime deleted succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        Console.Clear();
                        Console.WriteLine("Please enter the company's name you want to delete");
                        this.logic.DeleteCompany(Convert.ToInt32(Console.ReadLine()));
                        Console.WriteLine("Company deleted succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        Console.Clear();
                        Console.WriteLine("Please enter the Translator's name you want to delete");
                        this.logic.DeleteTranslator(Convert.ToInt32(Console.ReadLine()));
                        Console.WriteLine("Translator deleted succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.Escape: menuActive = false; Console.WriteLine("\nBack to the future!"); break;
                    default: Console.Clear(); Console.WriteLine("Wrong move"); Thread.Sleep(1000); break;
                }
            }
        }

        /// <summary>
        /// Menu for the deleting methods.
        /// </summary>
        private void DeleteMenu()
        {
            Console.Clear();
            string menu = "Choose one from below:\n" +
                            "1 - Delete Anime from the database\n" +
                            "2 - Delete Company from the database\n" +
                            "3 - Delete Translator from the database\n" +
                            "ESC - Back\n" +
                            "Choose one: ";
            Console.Write(menu);
        }

        /// <summary>
        /// Uptade entity from a table.
        /// </summary>
        private void UpdateEntity()
        {
            ConsoleKey pressedKey;
            bool menuActive = true;

            while (menuActive == true)
            {
                this.UpdateMenu();
                pressedKey = Console.ReadKey().Key;
                int id;
                switch (pressedKey)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        Console.Clear();
                        Console.WriteLine("Please enter the anime you want to update (identified by the ID)\n");
                        id = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Please enter the updated anime in this format:" +
                            "(Name_ReleaseAsYYYY - MM - DD_NumberOfEpisodes_Type_ScoreAs*.**_TranslatorID)");

                        this.logic.EditAnime(id, Console.ReadLine());
                        Console.WriteLine("Anime updated succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        Console.Clear();
                        Console.WriteLine("Please type in the company that you want to update (identified by the ID)\n");
                        id = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Please enter the updated company in this format:" +
                            "(Name_Location)");

                        this.logic.EditCompany(id, Console.ReadLine());
                        Console.WriteLine("Company updated succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        Console.Clear();
                        Console.WriteLine("Please type in the translator that you want to update (identified by the ID)\n");
                        id = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Please enter the updated translator in this format:" +
                            "(Name_BirthAsYYYY-MM-DD_CompanyID_SourceLanguage_TargetLanguage)");
                        this.logic.EditTranslator(id, Console.ReadLine());
                        Console.WriteLine("Translator updated succesfully");
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.Escape: menuActive = false; Console.WriteLine("\nBack to the future!"); break;
                    default: Console.Clear(); Console.WriteLine("Wrong move"); Thread.Sleep(1000); break;
                }
            }
        }

        /// <summary>
        /// Menu for the update methods.
        /// </summary>
        private void UpdateMenu()
        {
            Console.Clear();
            string menu = "Choose one from below:\n" +
                            "1 - Update an anime\n" +
                            "2 - Update a company\n" +
                            "3 - Update a translator\n" +
                            "ESC - Back\n" +
                            "Choose one: ";
            Console.Write(menu);
        }

        /// <summary>
        /// Display the queries.
        /// </summary>
        private void Queries()
        {
            ConsoleKey pressedKey;
            bool menuActive = true;

            while (menuActive == true)
            {
                this.QueryMenu();
                pressedKey = Console.ReadKey().Key;
                switch (pressedKey)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        Console.Clear();
                        Console.WriteLine(this.logic.TranslatorsAndAnimes());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        Console.Clear();
                        Console.WriteLine(this.logic.CompanyTranslatorCount());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        Console.Clear();
                        Console.WriteLine(this.logic.AnimesAboveEightScore());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:
                        Console.Clear();
                        Console.WriteLine(this.logic.JapaneseTranslatorsFromDavidProductionZrt());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.D5:
                    case ConsoleKey.NumPad5:
                        Console.Clear();
                        Console.WriteLine(this.logic.YoungTranslators());
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Thread.Sleep(1000);
                        break;
                    case ConsoleKey.Escape: menuActive = false; Console.WriteLine("\nBack to the future!"); break;
                    default: Console.Clear(); Console.WriteLine("Wrong move"); Thread.Sleep(1000); break;
                }
            }
        }

        /// <summary>
        /// Menu for the query methods.
        /// </summary>
        private void QueryMenu()
        {
            Console.Clear();
            string menu = "Choose one from below:\n" +
                            "1 - List the translated animes of translators\n" +
                            "2 - List the number of employees for each company\n" +
                            "3 - List the animes above 8.00 score\n" +
                            "4 - List the japanese translators from david production zrt.\n" +
                            "5 - List the young translators\n" +
                            "ESC - Back\n" +
                            "Choose one: ";
            Console.Write(menu);
        }
    }
}
