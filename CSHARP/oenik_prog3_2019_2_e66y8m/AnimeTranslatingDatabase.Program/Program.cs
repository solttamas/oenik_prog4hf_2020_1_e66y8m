﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AnimeTranslatingDatabase.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Program class, calls the DisplayConsole's constructor.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main method calling the console.
        /// </summary>
        /// <param name="args"> Argument. </param>
        public static void Main(string[] args)
        {
            DisplayConsole console = new DisplayConsole();
        }
    }
}
