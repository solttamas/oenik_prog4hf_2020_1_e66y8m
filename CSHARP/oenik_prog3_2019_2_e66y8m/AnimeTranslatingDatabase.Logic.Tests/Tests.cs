﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AnimeTranslatingDatabase.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AnimeTranslatingDatabase.AnimeLogic;
    using AnimeTranslatingDatabase.Data;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The Logic test's class.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        /*
        /// <summary>
        /// Testing the add anime method, but its commented because of a cultureinfo mental breakdown.
        /// </summary>
        [Test]
        public void TestingTheAddingOfAnAnime()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            Logic logic = new Logic(mockedrepo.Object);
            logic.InsertAnime("Kinai mese_1010-10-10_66_Special_6.67_6");
            mockedrepo.Verify(repo => repo.InsertAnime(It.IsAny<Anime>()), Times.Exactly(1));
        }*/

        /// <summary>
        /// Testing the add company method.
        /// </summary>
        [Test]
        public void TestingTheAddingOfACompany()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            Logic logic = new Logic(mockedrepo.Object);
            logic.InsertCompany("Test kft._Testland");
            mockedrepo.Verify(repo => repo.InsertCompany(It.IsAny<Company>()), Times.Exactly(1));
        }

        /// <summary>
        /// Testing the add translator method.
        /// </summary>
        [Test]
        public void TestingTheAddingOfATranslator()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            Logic logic = new Logic(mockedrepo.Object);
            logic.InsertTranslator("Testable Tamás_1998-08-17_1_Japán_Angol");
            mockedrepo.Verify(repo => repo.InsertTranslator(It.IsAny<Translator>()), Times.Exactly(1));
        }

        /*
        /// <summary>
        /// Testing the stringAnimes method, but its commented because of a cultureinfo mental breakdown.
        /// </summary>
        [Test]
        public void TestingTheListingOfAnimes()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>(MockBehavior.Loose);
            List<Anime> animes = new List<Anime>()
            {
                new Anime
                {
                    Name = "Kinai mese",
                    Release_Date = DateTime.Parse("1000-10-10"),
                    Episodes = 1,
                    Type = "TV",
                    Score = 6.66M,
                    Translator_ID = 1,
                },
            };
            mockedrepo.Setup(a => a.GetAnimes()).Returns(animes);
            Logic logic = new Logic(mockedrepo.Object);
            Assert.That(logic.StringAnimes, Is.EqualTo("Kinai mese 1000-10-10 1 TV 6.66 1\n"));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(1));
        }*/

        /// <summary>
        /// Testing the stringCompanies method.
        /// </summary>
        [Test]
        public void TestingTheListingOfCompanies()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Company> companies = new List<Company>()
            {
                new Company
                {
                    Name = "Test kft.",
                    Location = "Testland",
                },
            };
            mockedrepo.Setup(c => c.GetCompanies()).Returns(companies);
            Logic logic = new Logic(mockedrepo.Object);
            Assert.That(logic.StringCompanies(), Is.EqualTo("Test kft. Testland\n"));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(1));
        }

        /// <summary>
        /// Testing the stringTranslators method.
        /// </summary>
        [Test]
        public void TestingTheListingOfTranslators()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Translator> translators = new List<Translator>()
            {
                new Translator
                {
                    Name = "Testable Tamás",
                    Birth = DateTime.Parse("1998-08-17"),
                    Company_ID = 1,
                    Source_Language = "Angol",
                    Target_Language = "Magyar",
                },
            };
            mockedrepo.Setup(t => t.GetTranslators()).Returns(translators);
            Logic logic = new Logic(mockedrepo.Object);
            Assert.That(logic.StringTranslators(), Is.EqualTo("Testable Tamás 1998-08-17 1 Angol->Magyar\n"));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(1));
        }

        /*
        /// <summary>
        /// Testing the edit anime method, but its commented because of a cultureinfo mental breakdown.
        /// </summary>
        [Test]
        public void TestingTheEditingOfAnAnime()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Anime> animes = new List<Anime>()
            {
                new Anime
                {
                    Name = "Kinai mese",
                    Release_Date = DateTime.Parse("1000-10-10"),
                    Episodes = 1,
                    Type = "TV",
                    Score = 6.66M,
                    Translator_ID = 1,
                },
            };
            mockedrepo.Setup(a => a.GetAnimes()).Returns(animes);
            Logic logic = new Logic(mockedrepo.Object);
            logic.EditAnime("Kinai mese_1010-10-10_66_Special_6.67_6");
            Assert.That(logic.StringAnimes(), Is.EqualTo("Kinai mese 1010-10-10 66 Special 6.67 6\n"));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(2));
        }*/

        /// <summary>
        /// Testing the edit company method.
        /// </summary>
        [Test]
        public void TestingTheEditingOfACompany()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Company> companies = new List<Company>()
            {
                new Company
                {
                    Name = "Test kft.",
                    Location = "Testland",
                },
            };
            mockedrepo.Setup(c => c.GetCompanies()).Returns(companies);
            Logic logic = new Logic(mockedrepo.Object);
            logic.EditCompany("Test kft._Editland");
            Assert.That(logic.StringCompanies(), Is.EqualTo("Test kft. Editland\n"));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(2));
        }

        /// <summary>
        /// Testing the edit translator method.
        /// </summary>
        [Test]
        public void TestingTheEditingOfATranslator()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Translator> translators = new List<Translator>()
            {
                new Translator
                {
                    Name = "Testable Tamás",
                    Birth = DateTime.Parse("1998-08-17"),
                    Company_ID = 1,
                    Source_Language = "Angol",
                    Target_Language = "Magyar",
                },
            };
            mockedrepo.Setup(t => t.GetTranslators()).Returns(translators);
            Logic logic = new Logic(mockedrepo.Object);
            logic.EditTranslator("Testable Tamás_1998-08-18_2_Japán_Angol");
            Assert.That(logic.StringTranslators(), Is.EqualTo("Testable Tamás 1998-08-18 2 Japán->Angol\n"));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(2));
        }

        /// <summary>
        /// Testing the delete anime method.
        /// </summary>
        [Test]
        public void TestingTheDeletingOfAnAnime()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Anime> animes = new List<Anime>()
            {
                new Anime
                {
                    Name = "Kinai mese",
                },
            };
            mockedrepo.Setup(a => a.GetAnimes()).Returns(animes);
            Logic logic = new Logic(mockedrepo.Object);
            logic.DeleteAnime("Kinai mese");
            mockedrepo.Verify(repo => repo.DeleteAnime(It.IsAny<Anime>()), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(1));
        }

        /// <summary>
        /// Testing the delete company method.
        /// </summary>
        [Test]
        public void TestingTheDeletingOfACompany()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Company> companies = new List<Company>()
            {
                new Company
                {
                    Name = "Test kft.",
                },
            };
            mockedrepo.Setup(c => c.GetCompanies()).Returns(companies);
            Logic logic = new Logic(mockedrepo.Object);
            logic.DeleteCompany("Test kft.");
            mockedrepo.Verify(repo => repo.DeleteCompany(It.IsAny<Company>()), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(1));
        }

        /// <summary>
        /// Testing the delete translator method.
        /// </summary>
        [Test]
        public void TestingTheDeletingOfATranslator()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Translator> translators = new List<Translator>()
            {
                new Translator
                {
                    Name = "Testable Tamás",
                },
            };
            mockedrepo.Setup(t => t.GetTranslators()).Returns(translators);
            Logic logic = new Logic(mockedrepo.Object);
            logic.DeleteTranslator("Testable Tamás");
            mockedrepo.Verify(repo => repo.DeleteTranslator(It.IsAny<Translator>()), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(1));
        }

        /// <summary>
        /// Testing the translators and anime method.
        /// </summary>
        [Test]
        public void TestingTheTranslatorsAndAnimeMethod()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Translator> translators = new List<Translator>()
            {
                new Translator
                {
                    Translator_ID = 1,
                    Name = "Testable Tamás",
                },
                new Translator
                {
                    Translator_ID = 2,
                    Name = "Testable Tóbiás",
                },
                new Translator
                {
                    Translator_ID = 3,
                    Name = "Depression Dénes",
                },
            };
            List<Anime> animes = new List<Anime>()
            {
                new Anime
                {
                    Name = "Kinai mese",
                    Translator_ID = 2,
                    Type = "TV",
                },
                new Anime
                {
                    Name = "Japán mese",
                    Translator_ID = 1,
                    Type = "OVA",
                },
                new Anime
                {
                    Name = "Nem kinai mese",
                    Translator_ID = 1,
                    Type = "Special",
                },
            };
            mockedrepo.Setup(t => t.GetTranslators()).Returns(translators);
            mockedrepo.Setup(a => a.GetAnimes()).Returns(animes);
            Logic logic = new Logic(mockedrepo.Object);
            string output = logic.TranslatorsAndAnimes();
            string expected = "Testable Tamás:\n" +
                              "\tJapán mese - OVA\n" +
                              "\tNem kinai mese - Special\n" +
                              "Testable Tóbiás:\n" +
                              "\tKinai mese - TV\n" +
                              "Depression Dénes:\n";
            Assert.That(output, Is.EqualTo(expected));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(0));
        }

        /// <summary>
        /// Testing the company translator count method.
        /// </summary>
        [Test]
        public void TestingTheCompanyTranslatorCountMethod()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Translator> translators = new List<Translator>()
            {
                new Translator
                {
                    Name = "Testable Tamás",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Quackable Márton",
                    Company_ID = 2,
                },
                new Translator
                {
                    Name = "Depressable Gergő",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Sudoable Krisztián",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Catchable Alexa",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Drawable Petra",
                    Company_ID = 2,
                },
                new Translator
                {
                    Name = "Csinkable Csenki",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Marriageable Péter",
                    Company_ID = 2,
                },
            };
            List<Company> companies = new List<Company>()
            {
                new Company
                {
                    Company_ID = 1,
                    Name = "Failed zrt.",
                },
                new Company
                {
                    Company_ID = 2,
                    Name = "Passed zrt.",
                },
            };
            mockedrepo.Setup(t => t.GetTranslators()).Returns(translators);
            mockedrepo.Setup(c => c.GetCompanies()).Returns(companies);
            Logic logic = new Logic(mockedrepo.Object);
            string output = logic.CompanyTranslatorCount();
            string expected = "Failed zrt.: 5\n" +
                              "Passed zrt.: 3\n";
            Assert.That(output, Is.EqualTo(expected));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(0));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(1));
        }

        /// <summary>
        /// Testing the animes above eight score method.
        /// </summary>
        [Test]
        public void TestingTheAnimesAboveEightScoreMethod()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Anime> animes = new List<Anime>()
            {
                new Anime
                {
                    Name = "Japán mese",
                    Score = 9.99M,
                },
                new Anime
                {
                    Name = "Kinai mese",
                    Score = 6.66M,
                },
                new Anime
                {
                    Name = "Nem kinai mese",
                    Score = 8.00M,
                },
                new Anime
                {
                    Name = "Angol mese",
                    Score = 7.99M,
                },
            };
            mockedrepo.Setup(repo => repo.GetAnimes()).Returns(animes);
            Logic logic = new Logic(mockedrepo.Object);
            string output = logic.AnimesAboveEightScore();
            string expected = "Japán mese\n" +
                              "Nem kinai mese\n";
            Assert.That(output, Is.EqualTo(expected));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(0));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(0));
        }

        /// <summary>
        /// Testing the japanese translators from the david production zrt method.
        /// </summary>
        [Test]
        public void TestingTheJapaneseTranslatorsFromDavidProductionZrtMethod()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Translator> translators = new List<Translator>()
            {
                new Translator
                {
                    Name = "Testable Tamás",
                    Source_Language = "Japán",
                    Target_Language = "Angol",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Quackable Márton",
                    Source_Language = "Japán",
                    Target_Language = "Angol",
                    Company_ID = 2,
                },
                new Translator
                {
                    Name = "Depressable Gergő",
                    Source_Language = "Japán",
                    Target_Language = "Magyar",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Sudoable Krisztián",
                    Source_Language = "Japán",
                    Target_Language = "Magyar",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Catchable Alexa",
                    Source_Language = "Angol",
                    Target_Language = "Magyar",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Drawable Petra",
                    Source_Language = "Angol",
                    Target_Language = "Magyar",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Csinkable Csenki",
                    Source_Language = "Japán",
                    Target_Language = "Angol",
                    Company_ID = 1,
                },
                new Translator
                {
                    Name = "Marriageable Péter",
                    Source_Language = "Angol",
                    Target_Language = "Magyar",
                    Company_ID = 1,
                },
            };
            List<Company> companies = new List<Company>()
            {
                new Company
                {
                    Company_ID = 1,
                    Name = "David production kft.",
                },
                new Company
                {
                    Company_ID = 2,
                    Name = "Little duck zrt.",
                },
            };
            mockedrepo.Setup(t => t.GetTranslators()).Returns(translators);
            mockedrepo.Setup(c => c.GetCompanies()).Returns(companies);
            Logic logic = new Logic(mockedrepo.Object);
            string output = logic.JapaneseTranslatorsFromDavidProductionZrt();
            string expected = "Testable Tamás Angol\n" +
                              "Depressable Gergő Magyar\n" +
                              "Sudoable Krisztián Magyar\n" +
                              "Csinkable Csenki Angol\n";
            Assert.That(output, Is.EqualTo(expected));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(0));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(1));
        }

        /// <summary>
        /// Testing the young translators method.
        /// </summary>
        [Test]
        public void TestingTheYoungTranslatorsMethod()
        {
            Mock<Repository.Repository> mockedrepo = new Mock<Repository.Repository>();
            List<Translator> translators = new List<Translator>()
            {
                new Translator
                {
                    Name = "Testable Tamás",
                    Birth = DateTime.Parse("1998-08-17"),
                },
                new Translator
                {
                    Name = "Quackable Márton",
                    Birth = DateTime.Parse("1001-01-10"),
                },
                new Translator
                {
                    Name = "Depressable Gergő",
                    Birth = DateTime.Parse("2015-10-05"),
                },
                new Translator
                {
                    Name = "Sudoable Krisztián",
                    Birth = DateTime.Parse("2000-12-12"),
                },
                new Translator
                {
                    Name = "Catchable Alexa",
                    Birth = DateTime.Parse("1500-04-14"),
                },
                new Translator
                {
                    Name = "Drawable Petra",
                    Birth = DateTime.Parse("1998-02-11"),
                },
                new Translator
                {
                    Name = "Csinkable Csenki",
                    Birth = DateTime.Parse("1994-01-01"),
                },
                new Translator
                {
                    Name = "Marriageable Péter",
                    Birth = DateTime.Parse("1993-07-22"),
                },
            };
            mockedrepo.Setup(t => t.GetTranslators()).Returns(translators);
            Logic logic = new Logic(mockedrepo.Object);
            string output = logic.YoungTranslators();
            string expected = "Testable Tamás 21\n" +
                              "Depressable Gergő 4\n" +
                              "Sudoable Krisztián 19\n" +
                              "Drawable Petra 21\n" +
                              "Csinkable Csenki 25\n";
            Assert.That(output, Is.EqualTo(expected));
            mockedrepo.Verify(repo => repo.GetAnimes(), Times.Exactly(0));
            mockedrepo.Verify(repo => repo.GetTranslators(), Times.Exactly(1));
            mockedrepo.Verify(repo => repo.GetCompanies(), Times.Exactly(0));
        }
    }
}
