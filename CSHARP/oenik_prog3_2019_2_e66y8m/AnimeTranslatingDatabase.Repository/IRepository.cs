﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AnimeTranslatingDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AnimeTranslatingDatabase.Data;

    /// <summary>
    /// The IRepository interface.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// List the Animes table.
        /// </summary>
        /// <returns> List of the Animes. </returns>
        List<Anime> GetAnimes();

        /// <summary>
        /// List the Translators table.
        /// </summary>
        /// <returns> List of the Translators. </returns>
        List<Translator> GetTranslators();

        /// <summary>
        /// List the Companies table.
        /// </summary>
        /// <returns> List of the Companies. </returns>
        List<Company> GetCompanies();

        /// <summary>
        /// Insert a new anime.
        /// </summary>
        /// <param name="insert"> The anime what you want to insert. </param>
        void InsertAnime(Anime insert);

        /// <summary>
        /// Insert a new translator.
        /// </summary>
        /// <param name="insert">The translator who you want to insert. </param>
        void InsertTranslator(Translator insert);

        /// <summary>
        /// Insert a new company.
        /// </summary>
        /// <param name="insert"> The company what you want to insert. </param>
        void InsertCompany(Company insert);

        /// <summary>
        /// Editing an Anime.
        /// </summary>
        /// <param name="anime"> The edited anime. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        bool EditAnime(Anime anime);

        /// <summary>
        /// Editing a Translator.
        /// </summary>
        /// <param name="translator"> The edited translator. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        bool EditTranslator(Translator translator);

        /// <summary>
        /// Editing a Company.
        /// </summary>
        /// <param name="company"> The edited company. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        bool EditCompany(Company company);

        /// <summary>
        /// Deleting an anime.
        /// </summary>
        /// <param name="remove"> The anime that needs to be removed. </param>
        void DeleteAnime(Anime remove);

        /// <summary>
        /// Deleting a translator.
        /// </summary>
        /// <param name="remove"> The translator that needs to be removed. </param>
        void DeleteTranslator(Translator remove);

        /// <summary>
        /// Deleting a company.
        /// </summary>
        /// <param name="remove"> The company that needs to be removed. </param>
        void DeleteCompany(Company remove);
    }
}
