﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AnimeTranslatingDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AnimeTranslatingDatabase.Data;

    /// <summary>
    /// The repository class.
    /// </summary>
    public class Repository : IRepository
    {
        /// <summary>
        /// The database model.
        /// </summary>
        private AnimeDatabaseModel animeDatabase = new AnimeDatabaseModel();

        /// <summary>
        /// Deleting from animes.
        /// </summary>
        /// <param name="remove"> The anime what needs to be deleted. </param>
        public virtual void DeleteAnime(Anime remove)
        {
            this.animeDatabase.Animes.Remove(remove);
            this.animeDatabase.SaveChanges();
        }

        /// <summary>
        /// Deleting from companies.
        /// </summary>
        /// <param name="remove"> The company what needs to be deleted. </param>
        public virtual void DeleteCompany(Company remove)
        {
            this.animeDatabase.Companies.Remove(remove);
            this.animeDatabase.SaveChanges();
        }

        /// <summary>
        /// Deleting from translators.
        /// </summary>
        /// <param name="remove"> The translator what needs to be deleted. </param>
        public virtual void DeleteTranslator(Translator remove)
        {
            this.animeDatabase.Translators.Remove(remove);
            this.animeDatabase.SaveChanges();
        }

        /// <summary>
        /// Editing an anime.
        /// </summary>
        /// <param name="anime"> The anime that needs to be edited. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        public virtual bool EditAnime(Anime anime)
        {
            Anime update = this.animeDatabase.Animes.Single(a => a.Name == anime.Name);
            update.Release_Date = anime.Release_Date;
            update.Episodes = anime.Episodes;
            update.Type = anime.Type;
            update.Score = anime.Score;
            update.Translator_ID = anime.Translator_ID;
            this.animeDatabase.SaveChanges();
            return true;
        }

        /// <summary>
        /// Editing a company.
        /// </summary>
        /// <param name="company"> The company that needs to be edited. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        public virtual bool EditCompany(Company company)
        {
            Company update = this.animeDatabase.Companies.Single(c => c.Name == company.Name);
            update.Name = company.Name;
            update.Location = company.Location;
            return true;
        }

        /// <summary>
        /// Editing a translator.
        /// </summary>
        /// <param name="translator"> The translator that needs to be edited. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        public virtual bool EditTranslator(Translator translator)
        {
            Translator update = this.animeDatabase.Translators.Single(t => t.Name == translator.Name);
            update.Name = translator.Name;
            update.Birth = translator.Birth;
            update.Company_ID = translator.Company_ID;
            update.Source_Language = translator.Source_Language;
            update.Target_Language = translator.Target_Language;
            return true;
        }

        /// <summary>
        /// Inserting new anime.
        /// </summary>
        /// <param name="insert"> The anime that needs to be inserted. </param>
        public virtual void InsertAnime(Anime insert)
        {
            this.animeDatabase.Animes.Add(insert);
            this.animeDatabase.SaveChanges();
        }

        /// <summary>
        /// Inserting new company.
        /// </summary>
        /// <param name="insert"> The company that needs to be inserted. </param>
        public virtual void InsertCompany(Company insert)
        {
            this.animeDatabase.Companies.Add(insert);
            this.animeDatabase.SaveChanges();
        }

        /// <summary>
        /// Inserting new translator.
        /// </summary>
        /// <param name="insert"> The translator that needs to be inserted. </param>
        public virtual void InsertTranslator(Translator insert)
        {
            this.animeDatabase.Translators.Add(insert);
            this.animeDatabase.SaveChanges();
        }

        /// <summary>
        /// Creating a list from the Animes table.
        /// </summary>
        /// <returns> List of animes. </returns>
        public virtual List<Anime> GetAnimes()
        {
            List<Anime> animes = new List<Anime>();
            foreach (var anime in this.animeDatabase.Animes)
            {
                animes.Add(anime);
            }

            return animes;
        }

        /// <summary>
        /// Creating a list from the Companies.
        /// </summary>
        /// <returns> List of companies. </returns>
        public virtual List<Company> GetCompanies()
        {
            List<Company> companies = new List<Company>();
            foreach (var company in this.animeDatabase.Companies)
            {
                companies.Add(company);
            }

            return companies;
        }

        /// <summary>
        /// Creating a list from the Translators.
        /// </summary>
        /// <returns> List of translators. </returns>
        public virtual List<Translator> GetTranslators()
        {
            List<Translator> translators = new List<Translator>();
            foreach (var translator in this.animeDatabase.Translators)
            {
                translators.Add(translator);
            }

            return translators;
        }
    }
}
