﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnimeTranslatingDatabase.Web.Models
{
    public class AnimeViewModel
    {
        public AnimeW EditedAnime { get; set; }
        public List<AnimeW> ListOfAnimes { get; set; }
    }
}