﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AnimeTranslatingDatabase.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AnimeTranslatingDatabase.Data.Anime, AnimeTranslatingDatabase.Web.Models.AnimeW>()
                .ForMember(dest => dest.ID, map => map.MapFrom(src => src.Anime_ID))
                .ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name))
                .ForMember(dest => dest.Release, map => map.MapFrom(src => src.Release_Date))
                .ForMember(dest => dest.EpisodeCount, map => map.MapFrom(src => src.Episodes))
                .ForMember(dest => dest.Type, map => map.MapFrom(src => src.Type))
                .ForMember(dest => dest.Score, map => map.MapFrom(src => src.Score))
                .ForMember(dest => dest.Translator_ID, map => map.MapFrom(src => src.Translator_ID));
            });

            return config.CreateMapper();
        }
    }
}