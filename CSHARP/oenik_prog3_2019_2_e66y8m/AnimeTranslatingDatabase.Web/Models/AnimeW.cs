﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AnimeTranslatingDatabase.Web.Models
{
    public class AnimeW
    {
        [Display(Name = "Anime ID")]
        [Required]
        public int ID { get; set; }

        [StringLength(254)]
        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Release date")]
        [Required]
        public DateTime Release { get; set; }

        [Display(Name = "Episode count")]
        [Required]
        public int EpisodeCount { get; set; }

        [Display(Name = "Anime Type")]
        [Required]
        [StringLength(10)]
        public string Type { get; set; }

        [Display(Name = "Score")]
        [Required]
        public double Score { get; set; }

        [Display(Name = "Translator's ID")]
        [Required]
        public int? Translator_ID { get; set; }
    }
}