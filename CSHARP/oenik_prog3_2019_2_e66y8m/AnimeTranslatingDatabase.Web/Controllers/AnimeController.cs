﻿using System;
using System.Collections.Generic;
using AnimeTranslatingDatabase.AnimeLogic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AnimeTranslatingDatabase.Web.Models;
using AnimeTranslatingDatabase.Repository;
using System.ComponentModel.DataAnnotations;
using AnimeTranslatingDatabase.Data;

namespace AnimeTranslatingDatabase.Web.Controllers
{
    public class AnimeController : Controller
    {
        ILogic logic;
        IMapper mapper;
        AnimeViewModel avm;
        public AnimeController()
        {
            logic = new Logic();
            mapper = MapperFactory.CreateMapper();

            avm = new AnimeViewModel();
            avm.EditedAnime = new AnimeW();
            var animes = logic.GetAllAnime();
            avm.ListOfAnimes = mapper.Map<IList<Data.Anime>, List<Models.AnimeW>>(animes);
        }

        private AnimeW GetAnimeModel(int id)
        {
            Anime anime = logic.GetAllAnime().Where(x => x.Anime_ID == id).FirstOrDefault();
            return mapper.Map<Data.Anime, Models.AnimeW>(anime);
        }

        // GET: Anime
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("AnimeIndex", avm);
        }

        // GET: Anime/Details/5
        public ActionResult Details(int id)
        {
            return View("AnimeDetails", GetAnimeModel(id));
        }

        // POST: Anime/Remove/5
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Something went wrong with the delete";
            if (logic.DeleteAnime(id))
            {
                TempData["editResult"] = "Delete was succesful";
            }
            return RedirectToAction("Index");
        }

        // POST: Anime/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            avm.EditedAnime = GetAnimeModel(id);
            return View("AnimeIndex", avm);

        }

        // POST /Anime/Edit + Anime + editAction
        [HttpPost]
        public ActionResult Edit(AnimeW anime, string editAction)
        {
            if (ModelState.IsValid && anime != null)
            {
                TempData["editResult"] = "Succesful editing";
                if (editAction == "AddNew")
                {
                    logic.InsertAnime($"{anime.Name}_{anime.Release}_{anime.EpisodeCount}_{anime.Type}_{anime.Score}_{anime.Translator_ID}");
                }
                else
                {
                    if (!logic.EditAnime(anime.ID, $"{anime.Name}_{anime.Release}_{anime.EpisodeCount}_{anime.Type}_{anime.Score}_{anime.Translator_ID}"))
                    {
                        TempData["editResult"] = "Failed editing";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                avm.EditedAnime = anime;
                return View("AnimeIndex", avm);
            }           
        }       
    }
}
