﻿using AnimeTranslatingDatabase.AnimeLogic;
using AnimeTranslatingDatabase.Data;
using AnimeTranslatingDatabase.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AnimeTranslatingDatabase.Web.Controllers
{
    public class AnimeApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;
        public AnimeApiController()
        {
            logic = new Logic();
            mapper = MapperFactory.CreateMapper();

        }

        //GET api/AnimeApi
        [ActionName("all")]
        [HttpGet]
        //GET api/AnimeApi/all
        public IEnumerable<Models.AnimeW> GetAll()
        {
            var animes = logic.GetAllAnime();
            return mapper.Map<IList<Data.Anime>, List<Models.AnimeW>>(animes);
        }

        //GET api/AnimeApi/del/42
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneAnime(int id)
        {
            bool succes = logic.DeleteAnime(id);
            return new ApiResult() { OperationResult = succes };
        }
        //GET api/AnimeApi/add + anime
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneAnime(Anime anime)
        {
            bool succes = logic.InsertAnimetype(anime);
            return new ApiResult() { OperationResult = succes };
        }
        //GET api/AnimeApi/mod + anime
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneAnime(Anime anime)
        {
            bool succes = logic.EditAnime(anime.Anime_ID, $"{anime.Name}_{anime.Release_Date}_{anime.Episodes}_{anime.Type}_{anime.Score}_{anime?.Translator_ID}");
            return new ApiResult() { OperationResult = succes };
        }
    }
}
