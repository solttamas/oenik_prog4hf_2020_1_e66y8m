﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AnimeTranslatingDatabase.AnimeLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The ILogic interface.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// List the Animes table.
        /// </summary>
        /// <returns> List of the Animes. </returns>
        List<AnimeTranslatingDatabase.Data.Anime> GetAllAnime();

        /// <summary>
        /// List the Translators table.
        /// </summary>
        /// <returns> List of the Translators. </returns>
        List<AnimeTranslatingDatabase.Data.Translator> GetAllTranslator();

        /// <summary>
        /// List the Companies table.
        /// </summary>
        /// <returns> List of the Companies. </returns>
        List<AnimeTranslatingDatabase.Data.Company> GetAllCompany();

        /// <summary>
        /// Inserting a new anime by using a string.
        /// </summary>
        /// <param name="anime"> The string data what contains the anime. Needs to be splitted. </param>
        void InsertAnime(string anime);

        /// <summary>
        /// Inserting a new anime by using an Anime.
        /// </summary>
        /// <param name="anime"> The Anime what you want to insert. </param>
        /// <returns> Weather the insert was succesful or not. </returns>
        bool InsertAnimetype(AnimeTranslatingDatabase.Data.Anime anime);

        /// <summary>
        /// Inserting a new company by using a string.
        /// </summary>
        /// <param name="company"> The string data what contains the company. Needs to be splitted. </param>
        void InsertCompany(string company);

        /// <summary>
        /// Inserting a new translator by using a string.
        /// </summary>
        /// <param name="translator"> The string data what contains the translator. Needs to be splitted. </param>
        void InsertTranslator(string translator);

        /// <summary>
        /// Deleting an anime indentified by an ID.
        /// </summary>
        /// <param name="animeID"> The string what contains the anime's ID. </param>
        /// <returns> Weather the delete is succesful or not. </returns>
        bool DeleteAnime(int animeID);

        /// <summary>
        /// Deleting a company indentified an ID.
        /// </summary>
        /// <param name="companyID"> The string what contains the compnay's ID. </param>
        /// <returns> Weather the delete is succesful or not. </returns>
        bool DeleteCompany(int companyID);

        /// <summary>
        /// Deleting a translator indentified by an ID.
        /// </summary>
        /// <param name="translatorID"> The string what contains the translator's ID. </param>
        /// <returns> Weather the delete is succesful or not. </returns>
        bool DeleteTranslator(int translatorID);

        /// <summary>
        /// Editing an existing anime by using a string.
        /// </summary>
        /// <param name="animeID"> The int data what contains the anime's ID. </param>
        /// <param name="anime"> The string data what contains the anime. Needs to be splitted. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        bool EditAnime(int animeID, string anime);

        /// <summary>
        /// Editing an existing company by using a string.
        /// </summary>
        /// <param name="companyID"> The int data what contains the company's ID. </param>
        /// <param name="company"> The string data what contains the company. Needs to be splitted. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        bool EditCompany(int companyID, string company);

        /// <summary>
        /// Editing an existing translator by using a string.
        /// </summary>
        /// <param name="translatorID"> The int data what contains the translator's ID. </param>
        /// <param name="translator"> The string data what contains the translator. Needs to be splitted. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        bool EditTranslator(int translatorID, string translator);

        /// <summary>
        /// Creating a string from the list of animes.
        /// </summary>
        /// <returns> All anime in one string. </returns>
        string StringAnimes();

        /// <summary>
        /// Creating a string from the list of companies.
        /// </summary>
        /// <returns> All company in one string. </returns>
        string StringCompanies();

        /// <summary>
        /// Creating a string from the list of translators.
        /// </summary>
        /// <returns> All translator in one string. </returns>
        string StringTranslators();

        /// <summary>
        /// Adding the data from the java endpoint.
        /// </summary>
        /// <returns> The data of the inserted companies. </returns>
        string Java();

        /// <summary>
        /// Summary of the translator's translated animes.
        /// </summary>
        /// <returns> The translators with every anime what they translated in a string. </returns>
        string TranslatorsAndAnimes();

        /// <summary>
        /// Summary of the company's employed translators.
        /// </summary>
        /// <returns> The companies and their number of employees. </returns>
        string CompanyTranslatorCount();

        /// <summary>
        /// Lists all the anime above 8.00 score.
        /// </summary>
        /// <returns> All the anime above score 8.00. </returns>
        string AnimesAboveEightScore();

        /// <summary>
        /// Lists all the japanese translators from the david production zrt.
        /// </summary>
        /// <returns> All the japanese translator from the david profuction zrt. </returns>
        string JapaneseTranslatorsFromDavidProductionZrt();

        /// <summary>
        /// Lists the translators those who are not older than 25 years.
        /// </summary>
        /// <returns> The young translators. </returns>
        string YoungTranslators();
    }
}
