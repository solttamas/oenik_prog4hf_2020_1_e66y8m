﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AnimeTranslatingDatabase.AnimeLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using AnimeTranslatingDatabase.Data;

    /// <summary>
    /// The Logic's class.
    /// </summary>
    public class Logic : ILogic
    {
        /// <summary>
        /// The repository what the logic will be built on.
        /// </summary>
        private Repository.Repository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
            this.repository = new Repository.Repository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="repository"> The repository that needs to be used. </param>
        public Logic(Repository.Repository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Returns a list from the Animes table.
        /// </summary>
        /// <returns> List of animes. </returns>
        public virtual List<Anime> GetAllAnime()
        {
            return this.repository.GetAnimes();
        }

        /// <summary>
        /// Returns a list from the Companies table.
        /// </summary>
        /// <returns> List of companies. </returns>
        public virtual List<Company> GetAllCompany()
        {
            return this.repository.GetCompanies();
        }

        /// <summary>
        /// Returns a list from the Translators table.
        /// </summary>
        /// <returns> List of translators. </returns>
        public virtual List<Translator> GetAllTranslator()
        {
            return this.repository.GetTranslators();
        }

        /// <summary>
        /// Inserting a new anime by using a string.
        /// </summary>
        /// <param name="anime"> The string data what contains the anime. Needs to be splitted. </param>
        public virtual void InsertAnime(string anime)
        {
            string[] splitted = anime.Split('_');
            Anime insert = new Anime()
            {
                Name = splitted[0],
                Release_Date = DateTime.Parse(splitted[1]),
                Episodes = int.Parse(splitted[2]),
                Type = splitted[3],
                Score = Convert.ToDecimal(splitted[4]),
                Translator_ID = int.Parse(splitted[5]),
            };
            this.repository.InsertAnime(insert);
        }

        /// <summary>
        /// Inserting a new anime by using an Anime.
        /// </summary>
        /// <param name="anime"> The Anime what you want to insert. </param>
        /// <returns> Weather the insert was succesful or not. </returns>
        public virtual bool InsertAnimetype(Anime anime)
        {
            try
            {
                this.repository.InsertAnime(anime);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Inserting a new company by using a string.
        /// </summary>
        /// <param name="company"> The string data what contains the company. Needs to be splitted. </param>
        public virtual void InsertCompany(string company)
        {
            string[] splitted = company.Split('_');
            Company insert = new Company()
            {
                Name = splitted[0],
                Location = splitted[1],
            };
            this.repository.InsertCompany(insert);
        }

        /// <summary>
        /// Inserting a new translator by using a string.
        /// </summary>
        /// <param name="translator"> The string data what contains the translator. Needs to be splitted. </param>
        public virtual void InsertTranslator(string translator)
        {
            string[] splitted = translator.Split('_');
            Translator insert = new Translator()
            {
                Name = splitted[0],
                Birth = DateTime.Parse(splitted[1]),
                Company_ID = int.Parse(splitted[2]),
                Source_Language = splitted[3],
                Target_Language = splitted[4],
            };

            this.repository.InsertTranslator(insert);
        }

        /// <summary>
        /// Deleting an anime indentified by an ID.
        /// </summary>
        /// <param name="animeID"> The ID what contains the anime's ID. </param>
        /// <returns> Weather the delete is succesful or not. </returns>
        public virtual bool DeleteAnime(int animeID)
        {
            Anime delete = this.repository.GetAnimes().Single(a => a.Anime_ID == animeID);
            this.repository.DeleteAnime(delete);
            if (this.GetAllAnime().Where(x => x.Anime_ID == delete.Anime_ID).Count() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deleting a company indentified by an ID.
        /// </summary>
        /// <param name="companyID"> The string what contains the compnay's ID. </param>
        /// <returns> Weather the delete is succesful or not. </returns>
        public virtual bool DeleteCompany(int companyID)
        {
            Company delete = this.repository.GetCompanies().Single(c => c.Company_ID == companyID);
            this.repository.DeleteCompany(delete);
            if (this.GetAllCompany().Where(x => x.Company_ID == delete.Company_ID).Count() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deleting a translator indentified by the string.
        /// </summary>
        /// <param name="translatorID"> The string what contains the translator's ID. </param>
        /// <returns> Weather the delete is succesful or not. </returns>
        public virtual bool DeleteTranslator(int translatorID)
        {
            Translator delete = this.repository.GetTranslators().Single(t => t.Translator_ID == translatorID);
            this.repository.DeleteTranslator(delete);
            if (this.GetAllTranslator().Where(x => x.Translator_ID == delete.Translator_ID).Count() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Editing an existing anime by using a string.
        /// </summary>
        /// <param name="animeID"> The int data what contains the anime's ID. </param>
        /// <param name="anime"> The string data what contains the anime. Needs to be splitted. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        public virtual bool EditAnime(int animeID, string anime)
        {
            string[] splitted = anime.Split('_');
            Anime edit = this.repository.GetAnimes().Single(a => a.Name == splitted[0]);
            edit.Release_Date = DateTime.Parse(splitted[1]);
            edit.Episodes = int.Parse(splitted[2]);
            edit.Type = splitted[3];
            edit.Score = Convert.ToDecimal(splitted[4]);
            edit.Translator_ID = int.Parse(splitted[5]);
            return this.repository.EditAnime(edit);
        }

        /// <summary>
        /// Editing an existing company by using a string.
        /// </summary>
        /// <param name="companyID"> The int data what contains the company's ID. </param>
        /// <param name="company"> The string data what contains the company. Needs to be splitted. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        public virtual bool EditCompany(int companyID, string company)
        {
            string[] splitted = company.Split('_');
            Company edit = this.repository.GetCompanies().Single(c => c.Name == splitted[0]);
            edit.Location = splitted[1];
            return this.repository.EditCompany(edit);
        }

        /// <summary>
        /// Editing an existing translator by using a string.
        /// </summary>
        /// <param name="translatorID"> The int data what contains the translator's ID. </param>
        /// <param name="translator"> The string data what contains the translator. Needs to be splitted. </param>
        /// <returns> Weather the edit was succesful or not. </returns>
        public virtual bool EditTranslator(int translatorID, string translator)
        {
            string[] splitted = translator.Split('_');
            Translator edit = this.repository.GetTranslators().SingleOrDefault(t => t.Translator_ID == translatorID);
            edit.Birth = DateTime.Parse(splitted[1]);
            edit.Company_ID = int.Parse(splitted[2]);
            edit.Source_Language = splitted[3];
            edit.Target_Language = splitted[4];
            return this.repository.EditTranslator(edit);
        }

        /// <summary>
        /// Creating a string from the list of animes.
        /// </summary>
        /// <returns> All anime in one string. </returns>
        public virtual string StringAnimes()
        {
            string animes = "ID | Name | Release Date | Episodes | Type | Score | Translator ID\n";
            foreach (var anime in this.repository.GetAnimes())
            {
                animes += $"{anime.ToString()}\n";
            }

            return animes;
        }

        /// <summary>
        /// Creating a string from the list of companies.
        /// </summary>
        /// <returns> All company in one string. </returns>
        public virtual string StringCompanies()
        {
            string companies = "ID | Name | Location\n";
            foreach (var company in this.repository.GetCompanies())
            {
                companies += $"{company.ToString()}\n";
            }

            return companies;
        }

        /// <summary>
        /// Creating a string from the list of translators.
        /// </summary>
        /// <returns> All translator in one string. </returns>
        public virtual string StringTranslators()
        {
            string translators = "ID | Name | Birth | Company_ID | Source_Language -> Target_Language\n";
            foreach (var translator in this.repository.GetTranslators())
            {
                translators += $"{translator.ToString()}\n";
            }

            return translators;
        }

        /// <summary>
        /// Summary of the translator's translated animes.
        /// </summary>
        /// <returns> The translators with every anime what they translated in a string. </returns>
        public virtual string TranslatorsAndAnimes()
        {
            string translatorAndAnimes = string.Empty;
            List<Anime> animes = this.repository.GetAnimes();
            List<Translator> translators = this.repository.GetTranslators();
            foreach (var translator in translators)
            {
                translatorAndAnimes += $"{translator.Name}:\n";
                List<Anime> translatorsAnimes = animes.Where(a => a.Translator_ID == translator.Translator_ID).ToList();
                foreach (var anime in translatorsAnimes)
                {
                    translatorAndAnimes += $"\t{anime.Name} - {anime.Type}\n";
                }
            }

            return translatorAndAnimes;
        }

        /// <summary>
        /// Summary of the company's employed translators.
        /// </summary>
        /// <returns> The companies and their number of employees. </returns>
        public virtual string CompanyTranslatorCount()
        {
            string translatorCount = string.Empty;
            List<Company> companies = this.repository.GetCompanies();
            List<Translator> translators = this.repository.GetTranslators();
            foreach (var company in companies)
            {
                translatorCount += $"{company.Name}: ";
                var numberOfTranslators = (from t in translators
                                          group t by t.Company_ID into tg
                                          where tg.Key == company.Company_ID
                                          select tg.Count()).FirstOrDefault();
                translatorCount += $"{numberOfTranslators}\n";
            }

            return translatorCount;
        }

        /// <summary>
        /// Lists all the anime above 8.00 score.
        /// </summary>
        /// <returns> All the anime above score 8.00. </returns>
        public virtual string AnimesAboveEightScore()
        {
            string bestAnimes = string.Empty;
            var bestAnimesList = (from a in this.repository.GetAnimes()
                          where a.Score >= 8.00M
                          select a.Name).ToList();
            foreach (var goodAnime in bestAnimesList)
            {
                bestAnimes += $"{goodAnime}\n";
            }

            return bestAnimes;
        }

        /// <summary>
        /// Lists all the japanese translators from the david production zrt.
        /// </summary>
        /// <returns> All the japanese translator from the david profuction zrt. </returns>
        public virtual string JapaneseTranslatorsFromDavidProductionZrt()
        {
            var translators = from c in this.repository.GetCompanies()
                              join t in this.repository.GetTranslators()
                              on c.Company_ID equals t.Company_ID
                              where c.Name.ToLower() == "david production kft." && t.Source_Language.ToLower() == "japán"
                              select new
                              {
                                  TName = t.Name,
                                  TargetLanguage = t.Target_Language,
                              };
            string japaneseTranslators = string.Empty;
            foreach (var translator in translators)
            {
                japaneseTranslators += $"{translator.TName} {translator.TargetLanguage}\n";
            }

            return japaneseTranslators;
        }

        /// <summary>
        /// Lists the translators those who are not older than 25 years (in 2019).
        /// </summary>
        /// <returns> The young translators from 2019. </returns>
        public virtual string YoungTranslators()
        {
            string youngTranslators = string.Empty;
            var translators = from t in this.repository.GetTranslators()
                              where (2019 - t.Birth.Year) <= 25
                              select new
                              {
                                  TName = t.Name,
                                  Age = 2019 - t.Birth.Year,
                              };
            foreach (var translator in translators)
            {
                youngTranslators += $"{translator.TName} {translator.Age}\n";
            }

            return youngTranslators;
        }

        /// <summary>
        /// Adding a company from the data of the java endpoint.
        /// </summary>
        /// <returns> The data of the inserted companies. </returns>
        public virtual string Java()
        {
            XDocument javaXDoc = XDocument.Load("http://localhost:8080/oenik_prog3_2019_2_e66y8m/Servlet");
            var companies = from jdoc in javaXDoc.Descendants("company")
                            select new Company()
                            {
                                Name = jdoc.Element("name")?.Value,
                                Location = jdoc.Element("location")?.Value,
                            };
            string insertedCompanies = string.Empty;
            insertedCompanies += "Inserted companies:\n";
            foreach (var company in companies)
            {
                if (company.Name != null && company.Location != null)
                {
                    this.repository.InsertCompany(company);
                    insertedCompanies += "\t" + company.Name + " " + company.Location + "\n";
                }
            }

            insertedCompanies += "No more company remain";
            return insertedCompanies;
        }
    }
}
