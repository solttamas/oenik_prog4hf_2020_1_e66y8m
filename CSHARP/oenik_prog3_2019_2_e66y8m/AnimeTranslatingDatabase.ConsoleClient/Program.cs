﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AnimeTranslatingDatabase.ConsoleClient
{
    public class AnimeW
    {        
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime Release { get; set; }        
        public int EpisodeCount { get; set; }
        public string Type { get; set; }
        public double Score { get; set; }
        public int? Translator_ID { get; set; }

        public override string ToString()
        {
            return $"{ID} {Name} {Release} {EpisodeCount} {Type} {Score} {Translator_ID}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting....");
            Console.ReadLine();

            string url = "http://localhost:56011/api/AnimeApi/";
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<AnimeW>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item.ToString());
                }
                Console.ReadLine();
                Console.Clear();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(AnimeW.Name), "Teszt");
                postData.Add(nameof(AnimeW.Release), DateTime.Now.ToString());
                postData.Add(nameof(AnimeW.EpisodeCount), "69");
                postData.Add(nameof(AnimeW.Type), "TV");
                postData.Add(nameof(AnimeW.Score), "9.99");
                postData.Add(nameof(AnimeW.Translator_ID), "2");

                response = client.PostAsync(url + "add", 
                    new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);

                int animeID = JsonConvert.DeserializeObject<List<AnimeW>>(json).Single(x => x.Name == "Teszt").ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(AnimeW.ID), animeID.ToString());
                postData.Add(nameof(AnimeW.Name), "Teszt");
                postData.Add(nameof(AnimeW.Release), DateTime.Now.ToString());
                postData.Add(nameof(AnimeW.EpisodeCount), "69");
                postData.Add(nameof(AnimeW.Type), "TV");
                postData.Add(nameof(AnimeW.Score), "9.99");
                postData.Add(nameof(AnimeW.Translator_ID), "2");

                response = client.PostAsync(url + "mod",
                    new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                json = client.GetStringAsync(url + "del/" + animeID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();





            }

        }
    }
}
