﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeTranslatingDatabase.WPF
{
    class AnimeVM : ObservableObject
    {
		private int id;
		private string name;
		private DateTime release;
		private int episodeCount;
		private string type;
		private double score;
		private int? translator_ID;

		public int? Translator_ID
		{
			get { return translator_ID; }
			set { Set(ref translator_ID, value); }
		}

		public double Score
		{
			get { return score; }
			set { Set(ref score, value); }
		}

		public string Type
		{
			get { return type; }
			set { Set(ref type, value); }
		}

		public int EpisodeCount
		{
			get { return episodeCount; }
			set { Set(ref episodeCount, value); }
		}

		public DateTime Release
		{
			get { return release; }
			set { Set(ref release, value); }
		}

		public string Name
		{
			get { return name; }
			set { Set(ref name, value); }
		}

		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(AnimeVM other)
		{
			if (other == null) return;
			this.Id = other.Id;
			this.Name = other.Name;
			this.Release = other.Release;
			this.EpisodeCount = other.EpisodeCount;
			this.Type = other.Type;
			this.Score = other.Score;
			this.Translator_ID = other.Translator_ID;
		}
    }
}
