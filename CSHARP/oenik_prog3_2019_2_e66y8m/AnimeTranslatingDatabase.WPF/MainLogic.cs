﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AnimeTranslatingDatabase.WPF
{
    class MainLogic
    {
        string url = "http://localhost:56011/api/AnimeApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = "Failed operation";

            if (success)
            {
                msg = "Succesful operation";
            }
            Messenger.Default.Send(msg, "AnimeResult");
        }

        public List<AnimeVM> ApiGetAnimes()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<AnimeVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelAnime(AnimeVM anime)
        {
            bool success = false;
            if (anime != null)
            {
                string json = client.GetStringAsync(url + "del/" + anime.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditAnime(AnimeVM anime, bool isEditing)
        {
            if (anime == null) return false;
            string myUrl =url;
            if (isEditing)
            {
                myUrl += "mod";
            }
            else
            {
                myUrl += "add";
            }
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(AnimeVM.Id), anime.Id.ToString());
            postData.Add(nameof(AnimeVM.Name), anime.Name);
            postData.Add(nameof(AnimeVM.Release), anime.Release.ToString());
            postData.Add(nameof(AnimeVM.EpisodeCount), anime.EpisodeCount.ToString());
            postData.Add(nameof(AnimeVM.Type), anime.Type);
            postData.Add(nameof(AnimeVM.Score), anime.Score.ToString());
            postData.Add(nameof(AnimeVM.Translator_ID), anime.Translator_ID.ToString());

            string json = client.PostAsync(myUrl,
                new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditAnime(AnimeVM anime, Func<AnimeVM, bool> editor)
        {
            AnimeVM clone = new AnimeVM();
            if (anime != null) clone.CopyFrom(anime);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (anime != null) success = ApiEditAnime(clone, true);
                else ApiEditAnime(clone, false);
            }
            SendMessage(success == true);

        }
    }
}
