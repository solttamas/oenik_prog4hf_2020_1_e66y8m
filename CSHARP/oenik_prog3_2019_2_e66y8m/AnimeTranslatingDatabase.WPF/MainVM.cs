﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AnimeTranslatingDatabase.WPF
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private AnimeVM selectedAnime;
		private ObservableCollection<AnimeVM> allAnime;

		public ObservableCollection<AnimeVM> AllAnime
		{
			get { return allAnime; }
			set { Set(ref allAnime, value); }
		}

		public AnimeVM SelectedAnime
		{
			get { return selectedAnime; }
			set { Set(ref selectedAnime, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<AnimeVM, bool> EditorFunc { get; set; }
		public MainVM()
		{
			logic = new MainLogic();
			DelCmd = new RelayCommand(() => logic.ApiDelAnime(selectedAnime));
			AddCmd = new RelayCommand(() => logic.EditAnime(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditAnime(selectedAnime, EditorFunc));
			LoadCmd = new RelayCommand(() => 
				AllAnime = new ObservableCollection<AnimeVM>(logic.ApiGetAnimes()));


		}
	}
}
